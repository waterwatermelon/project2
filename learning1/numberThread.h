#pragma once
#include <iostream> 
#include <thread> 
#include  <mutex>
using namespace std;

class NumberList {
public:
	NumberList(int val)
	{
		m_num = val;
	}
	void printNum()
	{
		while (m_num <= 99)
		{
			this_thread::sleep_for(chrono::milliseconds(10));
			m_mtx.lock();
			cout << m_num << endl;
			m_num++;
			this_thread::sleep_for(chrono::milliseconds(10));
			m_mtx.unlock();
		}
	}
private:
	int m_num;
	mutex m_mtx;
};

void numberThreadtest();