#pragma once
#include <iostream>
#include  <unordered_map>
#include <string>
#include <memory>
using namespace std;

class Car
{
public:
	static unordered_map<string, int> brand_list;
	virtual void start()
	{
		cout << "Choose a brand!" << endl;
	}

};


class Tesla :public Car
{
public:
	void start() override
	{
		cout << "Safe driving!" << endl;
	}
};

class Porsche :public Car
{
public:
	void start() override
	{
		cout << "Nice car!" << endl;
	}
};

shared_ptr<Car> manufacture(const string& brand);
void Car_test();







