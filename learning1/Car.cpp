#include "Car.h"
unordered_map<string, int> Car::brand_list;
shared_ptr<Car> manufacture(const string& brand)
{
	shared_ptr<Car> p;
	int index = Car::brand_list.find(brand) == Car::brand_list.end() ? -1 : Car::brand_list[brand];
	switch (index)
	{
	case 0:
		p = make_shared<Tesla>();
		break;
	case 1:
		p = make_shared<Porsche>();
		break;
	default:
		p = make_shared<Car>();
		break;
	}
	return p;
}

void Car_test()
{
	Car::brand_list.insert(make_pair("Tesla", 0));
	Car::brand_list.insert(make_pair("Porsche", 1));
	shared_ptr<Car> myCar1 = manufacture("123");
	myCar1->start();
	shared_ptr<Car> myCar2 = manufacture("Tesla");
	myCar2->start();
	shared_ptr<Car> myCar3 = manufacture("Porsche");
	myCar3->start();
	system("pause");
}

